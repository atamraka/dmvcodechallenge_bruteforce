﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DmvAppointmentScheduler
{
    class Program
    {
        public static List<Appointment> appointmentList = new List<Appointment>();
        static void Main(string[] args)
        {
            CustomerList customers = ReadCustomerData();
            TellerList tellers = ReadTellerData();
            Calculation(customers, tellers);
            OutputTotalLengthToConsole();

        }
        private static CustomerList ReadCustomerData()
        {
            string fileName = "CustomerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            CustomerList customerData = JsonConvert.DeserializeObject<CustomerList>(jsonString);
            return customerData;

        }
        private static TellerList ReadTellerData()
        {
            string fileName = "TellerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            TellerList tellerData = JsonConvert.DeserializeObject<TellerList>(jsonString);
            return tellerData;

        }

        static (List<Teller>, List<String>) CheckTellerWithoutAnyMatchingCust(CustomerList customers, TellerList tellers){
             //check if there is any teller whose sepciality type doesnt match with any customer service type
            List<Teller> tellerWithNoMatchingCustomer=null;  
            List<String> uniqueTypeOfTellerWithNoMatchingCustomer=null;
            Console.WriteLine("Checking tellers without any matching customer type");
            Boolean found=false;
            foreach(Teller teller in tellers.Teller){
                string specialityType= teller.specialtyType;
                found=false;
                foreach(Customer customer in customers.Customer){
                    if(specialityType.Equals(customer.type)){
                        found=true;
                        break;
                    }
                }
                if(!found){
                    if(tellerWithNoMatchingCustomer==null){
                        tellerWithNoMatchingCustomer= new List<Teller>();
                    } 
                    if(uniqueTypeOfTellerWithNoMatchingCustomer==null){
                        uniqueTypeOfTellerWithNoMatchingCustomer= new List<string>();
                    }
                    if(!uniqueTypeOfTellerWithNoMatchingCustomer.Contains(specialityType)){
                        uniqueTypeOfTellerWithNoMatchingCustomer.Add(specialityType);
                    }
					tellerWithNoMatchingCustomer.Add(teller);
                }
            }
            return (tellerWithNoMatchingCustomer,uniqueTypeOfTellerWithNoMatchingCustomer);
        }

        static void Calculation(CustomerList customers, TellerList tellers)
        {
            (List<Teller> tellerWithNoMatchingCustomer,List<string> uniqueTypeOfTellerWithNoMatchingCustomer)= 
           CheckTellerWithoutAnyMatchingCust(customers, tellers);
          
             var tellerGroupBySpecType= tellers.Teller.Where(x=> !uniqueTypeOfTellerWithNoMatchingCustomer.Contains(x.specialtyType))
           .GroupBy(eachTeller => eachTeller.specialtyType);

             foreach(Customer customer in customers.Customer)
            {   
                Boolean match=false;
                Teller currentTellerToServe=null;
                 var appointment=(Appointment)null;
                foreach(var group in tellerGroupBySpecType){
                    //if customer's service type matches the tellergrpups' speciality type,
                    //get the teller with minimum time served
                     if(group.Key.Equals(customer.type)){
                         /** 
                            we need to sort the group  by ascending order of timeserved
                            so that each time, teller with lowest timeserved serves the matching customer
                            */
                         currentTellerToServe=group.OrderBy(eachTeller => eachTeller.timeServed).First(); //get the teller with lowest time Served
                         match=true;
                         
                         appointment = new Appointment(customer, currentTellerToServe);
                         currentTellerToServe.timeServed+= appointment.duration;
                         appointmentList.Add(appointment);                       
                         break;
                     }
                }
                if(!match){
                    //if customers service type didnt match with any teller, we can send them to the teller with no matching customer type
                    if(uniqueTypeOfTellerWithNoMatchingCustomer!=null && tellerWithNoMatchingCustomer.Any()){
                        /**
                        there can be multiple teller whose type doesnt match any customer. 
                        so how should we decide which teller shouls serve this customer?
                        So we take those teller who has no matching customer type and use those tellers although their type do not match

                        For logic of whom to chose as serving Teller,
                        we decide using same logic as above. The teller with minimum timeserved should serve the customer
                        */
                        tellerWithNoMatchingCustomer.Sort((x, y) => x.timeServed.CompareTo(y.timeServed));
                        currentTellerToServe=tellerWithNoMatchingCustomer.First();

                        appointment = new Appointment(customer, currentTellerToServe);
                        currentTellerToServe.timeServed+= appointment.duration;
                        appointmentList.Add(appointment);                
                    }
                }        
                Console.WriteLine("Customer with id {0} with serve Type {1} is being served by teller id {2} with speciality type {3}",customer.Id,customer.type, currentTellerToServe.id, currentTellerToServe.specialtyType);
            }
        }
        static void OutputTotalLengthToConsole()
        {
            var tellerAppointments =
                from appointment in appointmentList
                group appointment by appointment.teller into tellerGroup
                select new
                {
                    teller = tellerGroup.Key,
                    totalDuration = tellerGroup.Sum(x => x.duration),
                };
            var max = tellerAppointments.OrderBy(i => i.totalDuration).LastOrDefault();
            Console.WriteLine("Teller " + max.teller.id + " will work for " + max.totalDuration + " minutes!");
        }

    }
}
